package com.technome.devapps

class StoreService {

    boolean transactional = false

    def list(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Store"] ]
            return result
        }

        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        result.storeInstanceList = Store.list(params)
        result.storeInstanceTotal = Store.count()

        if(!result.storeInstanceList || !result.storeInstanceTotal)
            return fail(code:"default.list.failure")

        // Success.
        return result
    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Store", params.id] ]
            return result
        }

        result.storeInstance = Store.get(params.id)

        if(!result.storeInstance)
            return fail(code:"default.not.found")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Store", params.id] ]
            return result
        }

        result.storeInstance = Store.get(params.id)

        if(!result.storeInstance)
            return fail(code:"default.not.found")

        try {
            result.storeInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.delete.failure")
        }

    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Store", params.id] ]
            return result
        }

        result.storeInstance = Store.get(params.id)

        if(!result.storeInstance)
            return fail(code:"default.not.found")

        // Success.
        return result
    }

    def update(params) {
        Store.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.storeInstance && m.field)
                    result.storeInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["Store", params.id] ]
                return result
            }

            result.storeInstance = Store.get(params.id)

            if(!result.storeInstance)
                return fail(code:"default.not.found")

            // Optimistic locking check.
            if(params.version) {
                if(result.storeInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.storeInstance.properties = params

            if(result.storeInstance.hasErrors() || !result.storeInstance.save())
                return fail(code:"default.update.failure")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Store", params.id] ]
            return result
        }

        result.storeInstance = new Store()
        result.storeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.storeInstance && m.field) 
                result.storeInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["Store", params.id] ]
            return result
        }

        result.storeInstance = new Store(params)

        if(result.storeInstance.hasErrors() || !result.storeInstance.save(flush: true))
            return fail(code:"default.create.failure")

        // success
        return result
    }

}
