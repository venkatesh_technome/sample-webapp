package com.technome.devapps

import org.springframework.dao.DataIntegrityViolationException

class StoreController {

    def storeService

    

    
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']
def index = { redirect(action:list,params:params) }

    def list = {
            def result = storeService.list(params)
            if(!result.error) {
                return [ storeInstanceList: result.storeInstanceList,
                                storeInstanceTotal: result.storeInstanceTotal ]
            }

            flash.message = g.message(code: result.error.code, args: result.error.args)
            [storeInstanceList: Store.list(params), storeInstanceTotal: Store.count()]
    }



    def show = {
        def result = storeService.show(params)

        if(!result.error)
            return [ storeInstance: result.storeInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: list)
    }


    def delete = {
        def result = storeService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.delete.success", args: ["Store", params.id])
            redirect(action:list)
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found") {
            redirect(action:list)
            return
        }

        redirect(action:show, id: params.id)
    }


    def edit = {
        def result = storeService.edit(params)

        if(!result.error)
            return [ storeInstance : result.storeInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: list)
    }


    def update = {
        def result = storeService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.update.success", args: ["Store", params.id])
            redirect(action:show, id: params.id)
            return
        }

        if(result.error.code == "default.not.found") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:list)
            return
        }

        render(view:'edit', model:[storeInstance: result.storeInstance.attach()])
    }


    def create = {
        def result = storeService.create(params)

        if(!result.error)
            return [storeInstance: result.storeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: list)
    }


    def save = {
        def result = storeService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.create.success", args: ["Store", result.storeInstance.id])
            redirect(action:show, id: result.storeInstance.id)
            return
        }

        render(view:'create', model:[storeInstance: result.storeInstance])
    }

} // end class
