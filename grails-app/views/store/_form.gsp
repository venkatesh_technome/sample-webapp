<%@ page import="com.technome.devapps.Store" %>



<div class="fieldcontain ${hasErrors(bean: storeInstance, field: 'item', 'error')} required">
	<label for="item">
		<g:message code="store.item.label" default="Item" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="item" required="" value="${storeInstance?.item}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: storeInstance, field: 'details', 'error')} required">
	<label for="details">
		<g:message code="store.details.label" default="Details" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="details" required="" value="${storeInstance?.details}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: storeInstance, field: 'cost', 'error')} required">
	<label for="cost">
		<g:message code="store.cost.label" default="Cost" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cost" required="" value="${storeInstance?.cost}"/>
</div>

