package com.technome.devapps

class Store {

    String item
    String details
    String cost
    
    static constraints = {
    item(blank:false,minSize:2)
            details(blank:true,minSize:5)
             cost(blank:true)
    }
}
