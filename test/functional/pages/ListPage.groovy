package pages

import geb.Module

class ListPage extends ScaffoldPage {
	static url = "/store/list"
	
	static at = {
		title ==~ /Store List/
	}
	
	static content = {
		newStoreButton(to: CreatePage) { $("a", text: "New Store") }
		peopleTable { $("div.content table", 0) }
		storeRow { module StoreRow, storeRows[it] }
		storeRows(required: false) { peopleTable.find("tbody").find("tr") }
	}
}

class StoreRow extends Module {
	static content = {
		cell { $("td", it) }
		cellText { cell(it).text() }
        cellHrefText{ cell(it).find('a').text() }
		enabled { Boolean.valueOf(cellHrefText(0)) }
		firstName { cellText(1) }
		lastName { cellText(2) }
		showLink(to: ShowPage) { cell(0).find("a") }
	}
}