package com.technome.devapps

import pages.*
import org.junit.Test

class StoreCRUDTests  {

    @Test
    void doSomeCrud() {
         assert at(ListPage)
       
        newStoreButton.click()
        
        assert at(CreatePage)
        $("#enabled").click()
        item = "Mobile"
        cost = "21"
        createButton.click()
        
        assert at(ShowPage)
        assert enabled == true
        assert item == "Mobile"
        assert cost == "21"
        editButton.click()
        
        assert at(EditPage)
        $("#enabled").click()
        updateButton.click()
        
        assert at(ShowPage)
        
        
        assert at(ListPage)
        assert storeRows.size() == 1
        def row = storeRow(0)
        assert row.item == "Mobile"
        assert row.cost == "21"
        row.showLink.click()
        
        assert at(ShowPage)
        def deletedId = id
        withConfirm { deleteButton.click() }
        
        assert at(ListPage)
        assert message == "Store $deletedId deleted"
        assert storeRows.size() == 0
    }
}