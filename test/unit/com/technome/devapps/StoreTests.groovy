package com.technome.devapps



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Store)
class StoreTests {

     void testBlankItem(){
    def item = new Store(Item:"")
    assertFalse "there should be errors", item.validate()
    assertTrue "another way to check for errors after you call validate()", item.hasErrors()
  }

    void testBlankDetails(){
        def details = new Store(Details:"")
        assertFalse "there should be errors", details.validate()
    assertTrue "another way to check for errors after you call validate()", details.hasErrors()
    }
    
    void testDetails(){
         
    def details = new Store(Details:"aaa")
    assertFalse "datails should have minmum 10 characters", details.validate()
    assertTrue "another way to check for errors after you call validate()", details.hasErrors()
  }
  
    void testminItems(){
         
    def item = new Store(Item:"aa")
    assertFalse "Item name should have min 2 characters", item.validate()
    assertTrue "another way to check for errors after you call validate()", item.hasErrors()
  }
  
    
    
    //fail 
  void testmaxItem(){
      def item = new Store(Item:"aaaaaaaaaaaaaaaaaaaaaa")
    assertTrue "Item cannot exceed 20 characters", item.validate()
    assertFalse "another way to check for errors after you call validate()", item.hasErrors()
  }
  
   
    void testcost(){
      def cost = new Store(Cost:"aa")
    assertTrue "Cost should be Numeric", cost.validate()
    assertFalse "another way to check for errors after you call validate()", cost.hasErrors()
  }
  
    void testinsertItem(){
    Store hs = new Store(item:"mobile")
    assertEquals "mobile", hs.item
  }
    
    void testinsertDetails(){
    Store hs = new Store(details:"mobile details")
    assertEquals "mobile details", hs.details
  }
     void testinsertCost(){
    Store hs = new Store(cost:"21")
    assertEquals "21", hs.cost
  }
  
   
  
}
